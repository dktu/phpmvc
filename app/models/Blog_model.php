<?php

class Blog_model {
    private $dbh; //database handler
    private $stmt;

    public function __construct()
    {
        //data source name
        $dsn = 'mysql:host=localhost;dbname=phpmvc';
        try{
            $this->dbh = new PDO($dsn, 'root', '');
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }
/*   private $blog = [
        [
            "penulis"=> "Linux",
            "judul"=> "Belajar PHP MVC",
            "tulisan"=> "Tutorial PHP MVC"
        ],
        [
            "penulis"=> "Linux",
            "judul"=> "Belajar OOP PHP",
            "tulisan"=> "Tutorial PHP OOP"
        ],
        [
            "penulis"=> "Linux",
            "judul"=> "Belajar PHP Dasar",
            "tulisan"=> "Tutorial PHP Dasar"
        ],
    ];
*/
    public function getAllBlog()
    {
        $this->stmt = $this->dbh->prepare('SELECT * FROM blog');
        $this->stmt->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}