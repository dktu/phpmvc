<?php

class About extends Controller {
    public function index($nama = 'Dektu', $pekerjaan = 'SMKN 1 Denpasar')
    {
        $data['nama'] = $nama;
        $data['pekerjaan'] = $pekerjaan;
        $data['judul'] = 'User';
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }
    public function page()
    {
        $data['judul'] = 'Page';
        $this->view('templates/header');
        $this->view('about/page');
        $this->view('templates/footer');
    }
    
}